# Terminal Helpers v1.0

This is a set of terminal scripts and settings which would help you with easier access to some Linux functions. If you havent decided if this project is for you, read on about the Features below.

## What are the features?

* Extension mechanism for your settings in bash based on your machine name - therefor you can have different settings for eg. your working laptop and your server
* Helper for cleaning up your /boot folder from old kernels
* Helper for building and deploying maven, gradle, npm projects
* Helper for ssh connections to different servers
* Helper for searching file contents recursively
* Helper for searching through the terminal history
* Helper for some Linux commands not often used and therefor not remembered by me :)

## How do I get set up?

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

## Contribution guidelines

TBD, just talk to the person(s) below

## Who do I talk to?

* [Manuel Manhart](https://www.manhart.space)
